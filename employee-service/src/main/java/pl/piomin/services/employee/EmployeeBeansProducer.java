package pl.piomin.services.employee;

import com.orbitz.consul.Consul;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * 03/08/2021 - 19:20
 *
 * @author moussalmith@gmail.com
 * @project sample-quarkus-microservices
 */

@ApplicationScoped
public class EmployeeBeansProducer {

    @Produces
    Consul consulClient = Consul.builder().build();
}
