package pl.piomin.services.organization;

import com.orbitz.consul.Consul;
import com.orbitz.consul.HealthClient;
import com.orbitz.consul.model.health.ServiceHealth;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 03/08/2021 - 19:34
 *
 * @author moussalmith@gmail.com
 * @project sample-quarkus-microservices
 */
public class LoadBalancedFilter implements ClientRequestFilter {

    private Consul consulClient;
    private AtomicInteger counter = new AtomicInteger();

    public LoadBalancedFilter(Consul consulClient) {
        this.consulClient = consulClient;
    }

    @Override
    public void filter(ClientRequestContext ctx) {
        URI uri = ctx.getUri();
        HealthClient healthClient = consulClient.healthClient();
        List<ServiceHealth> instances = healthClient
                .getHealthyServiceInstances(uri.getHost()).getResponse();
        ServiceHealth instance = instances.get(counter.getAndIncrement());
        URI u = UriBuilder.fromUri(uri)
                .host(instance.getService().getAddress())
                .port(instance.getService().getPort())
                .build();
        ctx.setUri(u);
    }

}
