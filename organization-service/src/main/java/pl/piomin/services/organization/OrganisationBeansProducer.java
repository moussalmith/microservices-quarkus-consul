package pl.piomin.services.organization;

import com.orbitz.consul.Consul;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * 03/08/2021 - 19:32
 *
 * @author moussalmith@gmail.com
 * @project sample-quarkus-microservices
 */

@ApplicationScoped
public class OrganisationBeansProducer {
    @Produces
    Consul consulClient = Consul.builder().build();
}
