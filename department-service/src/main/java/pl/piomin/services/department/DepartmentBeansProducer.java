package pl.piomin.services.department;

import com.orbitz.consul.Consul;
import org.apache.http.client.utils.URIBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import pl.piomin.services.department.client.EmployeeClient;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import java.net.URISyntaxException;

/**
 * 03/08/2021 - 18:39
 *
 * @author moussalmith@gmail.com
 * @project sample-quarkus-microservices
 */

@ApplicationScoped
public class DepartmentBeansProducer {
/*    @ConfigProperty(name = "client.employee.uri")
    String employeeUri;*/
    @Produces
    Consul consulClient = Consul.builder().build();

    @Produces
    LoadBalancedFilter filter = new LoadBalancedFilter(consulClient);

/*    @Produces
    EmployeeClient employeeClient() throws URISyntaxException {
        URIBuilder builder = new URIBuilder(employeeUri);
        return RestClientBuilder.newBuilder()
                .baseUri(builder.build())
                .register(filter)
                .build(EmployeeClient.class);
    }*/

}
